# Front-End Assignment 1
## Cart Design

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)]()

Initial submission of the E-Commerse Cart design project.

## Installation

Frontend_DEV requires [Node.js](https://nodejs.org/) v12+ to run.

Install the dependencies and devDependencies and start the server.

```sh
cd Frontend_DEV
npm i
npm start
```