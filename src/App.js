import Cart from './components/cart.jsx'

function App() {
  return (
    
    <div className="App" style={{marginTop:'30px'}}>
      <Cart/>
    </div>
  );
}

export default App;
