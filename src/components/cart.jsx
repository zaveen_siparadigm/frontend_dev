import React from 'react';  
import './../App.css';
  
const Cart=()=> 
{ 
  return (
  
  <div className="container">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"/>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous"></link>

        <div className="row">
        <div className="col-md-9">
            <div class="row row-border">
            <div class="col-md-12">
                <div class="row cart-entry-row">
                    <div class="col-md-2" style={{textAlign:'left'}}>
                        <strong style={{color:"gray", fontSize:'12px'}}>PRODUCT</strong>
                    </div>
                    <div class="col-md-3">
                        {/* <strong style={{color:"gray"}}>Product</strong> */}
                    </div>
                    <div class="col-md-2" style={{textAlign:'left'}}>
                        <strong style={{color: 'gray', fontSize:'12px'}}>QUANTITY</strong>
                    </div>
                    <div class="col-md-2" style={{textAlign:'left'}}>
                        <strong style={{color: 'gray', fontSize:'12px'}}>PRICE</strong>
                    </div>
                    <div class="col-md-3">
                        {/* <strong style={{color: 'gray'}}>Price</strong> */}
                    </div>
                </div>
                <div class="row cart-entry-row">
                        <div class="col-md-2">
                            <img class="img-size" src="https://5.imimg.com/data5/PC/JR/MY-20619691/designer-men-shirt-500x500.jpg"/>
                        </div>
                        <div class="col-md-3">
                            <div class="row">
                                <div class="col" style={{textAlign:'left'}}>
                                    <strong>Gucci white and black shirt</strong>
                                    <p style={{fontSize:'13px', color:'gray'}}> Size: XL, Color: blue,<br></br>Brand: Gucci</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <select class="form-control">
                                <option>1</option>
                            </select>
                        </div>
                        <div class="col-md-2" style={{textAlign:'left'}}>
                            <div class="row">
                                <div class="col">
                                    <strong>$1156</strong>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <p style={{fontSize:'13px', color:'gray'}}>$315.20 each</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="heart-icon">
                                        <a><svg style={{marginTop:'7px'}} xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="gray" class="bi bi-heart-fill" viewBox="0 0 16 16">
                                            <path fill-rule="evenodd" d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"/>
                                        </svg></a>
                                    </div>
                                </div>  
                                <div class="col-md-3">
                                    <button style={{backgroundColor:'white', borderColor:'lightgrey', border: 'gray solid 1px'}} type="button" class="btn btn-light"><strong style={{fontWeight: '500'}}>Remove</strong></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row cart-entry-row">
                        <div class="col-md-2">
                            <img class="img-size" src="https://images-na.ssl-images-amazon.com/images/I/61IWnD7qL6L._AC_UL1500_.jpg"/>
                        </div>
                        <div class="col-md-3">
                            <div class="row">
                                <div class="col" style={{textAlign:'left'}}>
                                    <strong>Among Us Hoodie- "Very Sus"</strong>
                                    <p style={{fontSize:'13px', color:'gray'}}>Size: L-XL, Color: black,<br></br>Brand: Outfitters</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <select class="form-control">
                                <option>1</option>
                            </select>
                        </div>
                        <div class="col-md-2" style={{textAlign:'left'}}>
                        <div class="row">
                            <div class="col">
                                    <strong>$1156</strong>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <p style={{fontSize:'13px', color:'gray'}}>$315.20 each</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="row">
                            <div class="col-md-3">
                                    <div class="heart-icon">
                                        <a><svg style={{marginTop:'7px'}} xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="gray" class="bi bi-heart-fill" viewBox="0 0 16 16">
                                            <path fill-rule="evenodd" d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"/>
                                        </svg></a>
                                    </div>
                                </div>  
                                <div class="col-md-3">
                                    <button style={{backgroundColor:'white', borderColor:'lightgrey', border: 'gray solid 1px'}} type="button" class="btn btn-light"><strong style={{fontWeight: '500'}}>Remove</strong></button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row cart-entry-row">
                        <div class="col-md-2">
                            <img class="img-size" src="https://image.s5a.com/is/image/saks/0400099238439_A7?wid=480&hei=640&qlt=90&resMode=sharp2&op_usm=0.9,1.0,8,0"/>
                        </div>
                        <div class="col-md-3">
                            <div class="row">
                                <div class="col" style={{textAlign:'left'}}>
                                    <strong>Absolutely ripped jeans</strong>
                                    <p style={{fontSize:'13px', color:'gray'}}>Size: L, Color: light-blue,<br></br>Brand: Levis</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <select class="form-control">
                                <option>1</option>
                            </select>
                        </div>
                        <div class="col-md-2" style={{textAlign:'left'}}>
                            <div class="row" >
                                <div class="col">
                                        <strong>$1156</strong>
                                    </div>
                                </div>
                            <div class="row">
                                <div class="col">
                                    <p style={{fontSize:'13px', color:'gray'}}>$315.20 each</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="row">
                            <div class="col-md-3">
                                    <div class="heart-icon">
                                        <a><svg style={{marginTop:'7px'}} xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="gray" class="bi bi-heart-fill" viewBox="0 0 16 16">
                                            <path fill-rule="evenodd" d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"/>
                                        </svg></a>
                                    </div>
                                </div>  
                                <div class="col-md-3">
                                    <button style={{backgroundColor:'white', borderColor:'lightgrey', border: 'gray solid 1px'}} type="button" class="btn btn-light"><strong style={{fontWeight: '500'}}>Remove</strong></button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row cart-entry-row row-border" style={{borderBottom:'none', borderLeft:'none', borderRight:'none', borderRadius:'0px'}}>
                        <div class="col-md-9" style={{display:"flex", marginTop: '15px'}}>
                            <button class="btn btn-light" style={{width: '180px'}} type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                &lang; Continue Shopping
                            </button>
                        </div>
                        <div class="col-md-3" style={{display:"flex", marginTop: '15px'}}>
                            <button class="btn btn-primary" style={{width: '180px'}} type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                Make Purchase  &rang;
                            </button>
                        </div>
                    </div>

            </div>
            </div>
            <div class="row" style={{marginTop: '10px'}}>
                <div class="col-md-12">
                    <div class="alert alert-success" role="alert" style={{display:'flex'}}>
                        <img style={{width: '15px', height: '15px', marginTop:'5px'}} src="https://image.flaticon.com/icons/png/512/45/45880.png"/> &nbsp; Free Delivery within 1-2 weeks
                    </div>
                </div>
            </div>  
        </div>
        <div className="col-md-3">
            
        </div>

        </div>
  </div>   
    ); 
} 
  
export default Cart;